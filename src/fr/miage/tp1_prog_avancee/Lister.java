package fr.miage.tp1_prog_avancee;

import java.awt.Component; 
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


// Auteurs : Maxime Demetrio 

public class Lister {
	private static Component parent;
	
	public static void main(String[] args){
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF", "jpg","gif");
		FileNameExtensionFilter filter2 = new FileNameExtensionFilter("PNG", "png");
		chooser.setFileFilter(filter);
		chooser.setFileFilter(filter2);
		int returnVal=chooser.showOpenDialog(parent);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			System.out.println("Vous avez choisis d'ouvrir : "+ chooser.getSelectedFile().getName());
		}
	}

}